<?php

namespace App\Traits;

use App\User;

trait StoreImageTrait
{
    public function userBan($userID)
    {
        $user = User::find($userID);
        $arr['ban'] = true;
        $user->update($arr);
    }
}
