<?php

namespace App\Http\Middleware;

use App\Role;
use Closure;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userID=Auth::user()->id;
        if (Role::find($userID)->where('permission', 'Admin')) {
            return redirect('home');
        }
        return $next($request);
    }
}
