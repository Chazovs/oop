<?php

namespace App\Http\Middleware;

use App\Role;
use Closure;

class CheckOrganizer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userID=Auth::user()->id;
        if (Role::find($userID)->where('permission', 'Organizer')) {
            return redirect('home');
        }
        return $next($request);
    }
}
