<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Traits\Ban;

class AdminController extends Controller
{
    use Ban;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin.check');
    }

    public function userUpdate($userID, $arr)
    {
        $user = User::find($userID);
        $user->update($arr);
    }

    public function userDelete($userID)
    {
        $user = User::find($userID);
        $user->delete();
    }



    public function addOrganizer($userID)
    {
        $newRole['permission'] = 'Orginizer';
        $newRole['id'] = $userID;
        Role::firstOrCreate($newRole);
    }

    public function delOrganizer($userID)
    {
        $role = Role::where([['user_id', '=', $userID], ['permission', '=', 'Orgonizer']]);
        $role->delete();

    }

}
