<?php

namespace App\Http\Controllers;

use App\Traits\Ban;
use http\Client\Curl\User;

class OrganizerController extends Controller
{
    use Ban;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('org.check');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addInvite($userID, $activityID)
    {
        $user = User::find($userID);
        $user->Activities()->attach($activityID);
    }
}
